def main():
    x= readSingleLine(open('simple.txt'),3)
    y= readSingleLine(open('simple.txt'),4)
    name= readSingleLine(open('simple.txt'),5)
    print(int(x) + int(y),name*3)

def readSingleLine(fh,offset):
    line = fh.readlines()
    line = line[offset].split('=')
    v = line[1].rstrip('\n')
    return v

if __name__ == '__main__':
    main()