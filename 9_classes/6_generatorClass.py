class inclusive_range:
    def __init__(self,*args):
        numArgs = len(args)
        if numArgs < 1:
            raise TypeError('Requires at least one argument')
        elif numArgs == 1:
            self.start = 0
            self.stop = args[0]
            self.step = 1
        elif numArgs == 2:
            (self.start,self.stop) = args
            self.step = 1
        elif numArgs == 3:
            (self.start,self.stop,self.step) = args
        else:
            raise TypeError('Expected 3 args there are {}'.format(numArgs))

    def __iter__(self):
        i = self.start
        while i <= self.stop:
            yield i
            i += self.step
def main():
    try:
        o = inclusive_range(45)
    except TypeError as e:
        print(e)
    else:
        for x in o:
            print (x, end=' ')

if __name__ == '__main__':main()