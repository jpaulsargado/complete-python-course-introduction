class Instructor:
    def __init__(self,fname="Kevin",lname="Lastimosa",age=17,dept="ICT",ratePerHour=95):
        self.firstname = fname
        self.lastname = lname
        self.age = age
        self.department = dept
        self.ratePerHour = ratePerHour

    def getProfileInfo(self):
        info = {"firstname":self.firstname,"lastname":self.lastname,
                "age":self.age,"department":self.department,"rate":self.ratePerHour}
        return info
def main():
    ictInstructor = Instructor("Jerry","Doe",76,"ICT",780)
    profile = ictInstructor.getProfileInfo()
    for key in profile:
        print(profile[key])
if __name__ == '__main__':main()