# I = Prt
# I = amount of interest earned
# P = principal sum of money earning the interest
# r = the anual(or nominal) interest rate (usally expressed as a percentage)
# t = the interest period in years
# S = P(1 + rt)
# S = the future value
# interest = []
def simpleInterest():
	principalAmout = int(input("Enter Principal Amout: "))
	firstInterest = int(input("Enter Interest: "))
	years = int(input("Enter period in years: "))
	P = principalAmout #principal amount entered
	i = firstInterest / 100 #first interest
	t = years #period in years
	percentageList = []
	for x in range(1,t+1):
		percentageAdded = x / 100 #perce
		percentagePerYear = round(i+percentageAdded,2)
		percentageList.append(percentagePerYear)
		S = P * (1 + (percentagePerYear * t))
		# print("First Interest: {} added interest is {} = {}".format(i, percentageAdded,percentagePerYear) )
		print("Interest in {} yr/s is {} ".format(x,round(S,2) ) )
	print('{}'.format(percentageList) )
	print('Total interest {}%'.format( round(sum(percentageList) * 100),2) )
simpleInterest()