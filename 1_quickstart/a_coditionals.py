# Conditionals
name,age = "Zach Timothy Aaron",18

if age <= 20:
	print('{} - {}'.format(name,age))
else:
	print ("Over 20")
# print((condition) ? "True" : "False")
print("Young and free" if age < 20 else "Work Hard!")