def main():
    try:
       for i in readFile("simple.py"):
           print(i)
    except IOError as e:
        print("Could not open file: ",e)
    except ValueError as e:
        print("Bad file name: ",e)
def readFile(filename):
    if filename.endswith('txt'):
        fh = open(filename)
        return fh.readlines()
    else:
        raise ValueError('File name must end with .txt')
if __name__ == '__main__':
    main()