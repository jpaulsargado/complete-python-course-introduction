'''Prints the running times for problem sizes that double, using a single loop'''

import time

problemSize = 1000000
print("%12s%16s" % ("Problem size","Seconds"))
for count in range(5):
    start = time.time()
    # Start of the algorithm
    work = 1
    for x in range(problemSize):
        work +=1
        work -=1

    # end of the algorithm
    elapsed = time.time() - start
    print("%12d%15.3f" % (problemSize, elapsed))
    problemSize *=2